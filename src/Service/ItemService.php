<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Item;
use Doctrine\ORM\EntityManagerInterface;

use App\Dto\Entity\Item as ItemDto;

class ItemService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(ItemDto $itemDto): void
    {
        $item = new Item();
        $item->setUser($itemDto->getUser());
        $item->setData($itemDto->getData());

        $this->entityManager->persist($item);
        $this->entityManager->flush();
    }

    public function update(ItemDto $itemDto): void
    {
        $item = $this->entityManager->getRepository(Item::class)
            ->findOneBy([
                'id' => $itemDto->getId(),
                'user' => $itemDto->getUser(),
            ]);

        $item->setData($itemDto->getData());

        $this->entityManager->persist($item);
        $this->entityManager->flush();
    }

	public function delete(ItemDto $itemDto): void
	{
        $item = $this->entityManager->getRepository(Item::class)
            ->findOneBy([
                'id' => $itemDto->getId(),
                'user' => $itemDto->getUser(),
            ]);

        $this->entityManager->remove($item);
        $this->entityManager->flush();
	}
} 
