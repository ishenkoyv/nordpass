<?php

namespace App\Dto\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\IsNull;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;

use App\Component\Dto\Entity\DtoInterface;
use App\Component\Dto\Entity\DtoIdInterface;
use App\Component\Dto\Entity\DtoUserInterface;
use App\Component\Dto\Validator\EntityExists;

use App\Entity\User;

class Item implements DtoIdInterface, DtoUserInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var User 
     */
    protected $user;

    /**
     * @var string
     */
	protected $data;

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('id', new IsNull(['groups' => ['OpCreate']]));
        $metadata->addPropertyConstraint('id', new NotNull(['groups' => ['OpUpdate', 'OnDelete']]));
		$metadata->addConstraint(new EntityExists([
			'entityClass' => \App\Entity\Item::class,
			'properties' => ['id', 'user'],
			'groups' => ['OpUpdate', 'OnDelete'],
		]));

        $metadata->addPropertyConstraint('data', new NotBlank(['groups' => ['OpCreate', 'OpUpdate']]));
        $metadata->addPropertyConstraint('data', new Assert\Length([
            'min'    => 5,
            'max'    => 1000,
            'groups' => ['OpCreate', 'OpUpdate'],
        ]));
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return User 
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user 
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
        $this->user_id = $user ? $user->getId() : null;
    }

    /**
     * @return string
     */
	public function getData(): ?string
	{
        return $this->data;
    }

    /**
     * @param string $data
     *
     * @return self
     */
    public function setData(?string $data): Item
    {
        $this->data = $data;

        return $this;
    }
}
