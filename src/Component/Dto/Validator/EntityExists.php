<?php

namespace App\Component\Dto\Validator;

use Symfony\Component\Validator\Constraint;

/**
 */
class EntityExists extends Constraint
{
    public const NOT_EXIST_ERROR = '563a9f11-b5b6-4668-be2f-63117a4b61e8';

    public $message = 'Item does not exist or you don\'t have permission to access it';

    /**
     * @var string
     */
    public $entityClass;
    public $properties = [];
    public $repositoryMethod = 'findBy';

    protected static $errorNames = [
        self::NOT_EXIST_ERROR => 'NOT_EXIST_ERROR',
    ];

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions(): array
    {
        return ['entityClass', 'properties'];
    }


    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return [self::CLASS_CONSTRAINT, self::PROPERTY_CONSTRAINT];
    }
}
