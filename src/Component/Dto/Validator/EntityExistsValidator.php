<?php

namespace App\Component\Dto\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Check if an entity exists in the database.
 */
class EntityExistsValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($object, Constraint $constraint): void
    {
        if (!$constraint instanceof EntityExists) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\EntityExists');
        }

		$object = $this->context->getObject();

        $properties = (array) $constraint->properties;

        /**
         * @var $class \Doctrine\ORM\Mapping\ClassMetadata
         */
        $class = $this->entityManager->getClassMetadata($constraint->entityClass);

        $criteria = [];
        foreach ($properties as $propertyName) {
            if (!$class->hasField($propertyName) && !$class->hasAssociation($propertyName)) {
                throw new ConstraintDefinitionException(
                    sprintf('The field "%s" is not mapped by Doctrine, so it cannot be validated for existence.', $propertyName)
                );
            }

            $criteria[$propertyName] = $object->{'get' . ucfirst($propertyName)}();
        }

        $repository = $this->entityManager->getRepository($constraint->entityClass);

        $result = $repository->{$constraint->repositoryMethod}($criteria);

        if (0 !== count($result)) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setCode(EntityExists::NOT_EXIST_ERROR)
            ->addViolation();
    }
}
