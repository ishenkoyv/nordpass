<?php

namespace App\Component\Dto\Entity;

interface DtoIdInterface
{
    /**
     * Returns a resource identifier.
     *
     * @return int|null If identifier is `null`, that's a new resource
     *                  and you can only perform a "create" operation on it.
     */
    public function getId(): ?int;

    /**
     * Set the resource identifier.
     *
     * @param int $id
     */
    public function setId(int $id): void;
}
