<?php

namespace App\Component\Dto\Entity;

use App\Entity\User;

interface DtoUserInterface
{
    /**
     * @return User 
     */
    public function getUser(): User;

    /**
     * @param User 
     */
    public function setUser(User $user): void;
}
