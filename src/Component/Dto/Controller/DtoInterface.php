<?php

namespace App\Component\Dto\Controller;

interface DtoInterface
{
    public function getDtoClassName(): string;
}
