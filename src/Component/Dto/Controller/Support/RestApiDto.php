<?php

namespace App\Component\Dto\Controller\Support;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use App\Component\Dto\Entity\DtoUserInterface;
use App\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

trait RestApiDto
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
	 * @required
	 * @param Symfony\Component\Serializer\SerializerInterface $serializer
	 */
	public function setSerializer(\Symfony\Component\Serializer\SerializerInterface $serializer): void
	{
        $this->serializer = $serializer;
	}

    /**
	 * @required
	 * @param Symfony\Component\Validator\Validator\ValidatorInterface $validator
	 */
	public function setValidator(\Symfony\Component\Validator\Validator\ValidatorInterface $validator): void
	{
        $this->validator = $validator;
	}
	

    /**
     * @return string 
     */
    public function getDtoClassName(): string
    {
        return $this->dtoClassName;
    }


    public function getRequestDto(Request $request)
    {
		$requestDto = $this->serializer->deserialize(
            $request->getContent() ?: json_encode(array_merge($request->request->all(), $request->query->all())),
            $this->getDtoClassName(),
            'json'
        );

        if ($requestDto instanceof DtoUserInterface) {
			$requestDto->setUser($this->getUser());
        }

		return $requestDto;
    }

    /**
     * @throws UnprocessableEntityHttpException
     */
    public function validateRequestDto($requestDto, $validationTypes = []): void
    {
		$errors = $this->validator->validate($requestDto, null, $validationTypes);

        if (\count($errors) > 0) {
            throw new UnprocessableEntityHttpException($errors);
        }
    }
}
