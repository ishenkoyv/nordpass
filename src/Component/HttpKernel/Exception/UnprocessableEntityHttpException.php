<?php

namespace App\Component\HttpKernel\Exception;

use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException as BaseUnprocessableEntityHttpException;

class UnprocessableEntityHttpException extends BaseUnprocessableEntityHttpException implements RestApiGeneratedExceptionInterface
{
}
