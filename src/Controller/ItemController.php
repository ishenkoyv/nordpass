<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Component\Dto\Controller\Support\RestApiDto as RestApiDtoCrudTrait;
use App\Component\Dto\Controller\DtoInterface as ControllerDtoInterface;
use App\Component\HttpKernel\Exception\RestApiGeneratedExceptionInterface;
use App\Entity\Item;
use App\Service\ItemService;
use App\Dto\Entity\Item as ItemDto;

class ItemController extends AbstractController implements ControllerDtoInterface
{
    use RestApiDtoCrudTrait;

	protected $dtoClassName = ItemDto::class;

    /**
     * @Route("/item", name="item_list", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function list(): JsonResponse
    {
        $items = $this->getDoctrine()->getRepository(Item::class)->findBy(['user' => $this->getUser()]);

        $allItems = [];
        foreach ($items as $item) {
            $oneItem['data'] = $item->getData();
            $oneItem['created_at'] = $item->getCreatedAt();
            $oneItem['updated_at'] = $item->getUpdatedAt();
            $allItems[] = $oneItem;
        }

        return $this->json($allItems);
    }

    /**
     * @Route("/item", name="item_create", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function create(Request $request, ItemService $itemService)
    {
		// Deprecated. Only for back compability
		if (empty($request->get('data'))) {
			return $this->json(['error' => 'No data parameter']);
		}

        try {
            $itemDto = $this->getRequestDto($request);

            $this->validateRequestDto($itemDto, ['OpCreate']);

            $itemService->create($itemDto);
        } catch (\Exception $e) {
            if ($e instanceof RestApiGeneratedExceptionInterface) {
                throw $e;
            }

            throw new HttpException(500, 'Error occurred while trying to create the resource.');
        }

        return $this->json([]);
    }

    /**
     * @Route("/item", name="item_update", methods={"PUT"})
     * @IsGranted("ROLE_USER")
     */
    public function update(Request $request, ItemService $itemService)
    {
        try {
			$itemDto = $this->getRequestDto($request);

			$this->validateRequestDto($itemDto, ['OpUpdate']);

			$itemService->update($itemDto);
        } catch (\Exception $e) {
            if ($e instanceof RestApiGeneratedExceptionInterface) {
                throw $e;
            }

            throw new HttpException(500, 'Error occurred while trying to create the resource.');
        }

		return $this->json([]);
    }

    /**
     * @Route("/item", name="items_delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     */
    public function delete(Request $request, ItemService $itemService)
    {
		$itemDto = $this->getRequestDto($request);

		$this->validateRequestDto($itemDto, ['OpDelete']);

        try {
			$itemService->delete($itemDto);
        } catch (\Exception $e) {
            throw new HttpException(500, 'Error occurred while trying to delete the resource.');
        }

        return $this->json([]);
    }
}
