# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keepachangelog] and this project adheres to [Semantic Versioning][semver].

-----------------------

## [Unreleased]

## [0.0.1] - 2021-01-21
### Changed
- Added docker/mysql/data to .gitignore
- Fixed phpunit configuration issues related to phpunit.xml
- Fixed phpunit test errors
- Implemented simple DTO(Data Transfer Object) backbone code for data fetching from request, validation
- Added RestApiDto trait to make creation of new REST API controllers easier
- Custom EntityExists validor
- Error generation/processing through Exceptions
- Specific REST API exceptions (implements RestApiGeneratedExceptionInterface)
- Doctrine is configured to use pool based on array cache adapter for performance purposes
- Refactored ItemController
- Check for equality of logged in User and Item.user before Item entity modification
- Added Item update REST API endpoint
- Added a phpunit functional test for Item update REST API endpoint

[keepachangelog]:https://keepachangelog.com/en/1.0.0/
[semver]:https://semver.org/spec/v2.0.0.html
