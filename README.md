# Secure Information Storage REST API

This is a test project provided by the NordPass team.

Check CHANGELOG.md file for the latest changes, current codebase version, etc. Please bear in mind, that there is still enough room to make the application faster/secure,
e.g. additional caching (data checks inside entities, entity repositories result caching and cache data invalidation, replacement of array cache adapter with more reliable one).


### Project setup

* Add `secure-storage.localhost` to your `/etc/hosts`: `127.0.0.1 secure-storage.localhost`

* Run `make init` to initialize project

* Open in browser: http://secure-storage.localhost:8000/item Should get `Full authentication is required to access this resource.` error, because first you need to make `login` call (see `postman_collection.json` or `SecurityController` for more info).

### Run tests

make tests

### API credentials

* User: john
* Password: maxsecure

### API endpoints

**Login**
----

`POST /login`

  * **Data Params**

    **Required:**
 
     `username=[string]`
	 
     `password=[string]`
	 
   
  * **Success Response:**
  
    **Code:** 200
  
    **Content:** `{"username": "john", "roles": ["ROLE_USER"]}`
	
**Item**
----


`GET /item`

  * **Description:** Retrieve user's items
	
  * **Success Response:**
  
    **Code:** 200
  
    **Content:** `[{"data":"very secure new item data","created_at":"2021-01-20T18:29:35+00:00","updated_at":"2021-01-21T00:19:35+00:00"}]`
	
`POST /item`

  * **Description:** Create item
  
  * **Data Params**

    **Required:**
 
     `data=[string]`
	
  * **Success Response:**
  
    **Code:** 200
  
    **Content:** `[]`
	
`PUT /item`

  * **Description:** Update item
  
  * **Request Params**

    **Required:**
 
     `id=[integer]`
     `data=[string]`
	
  * **Success Response:**
  
    **Code:** 200
  
    **Content:** `[]`
	
`DELETE /item`

  * **Description:** Delete item
  
  * **Request Params**

    **Required:**
 
     `id=[integer]`
	
  * **Success Response:**
  
    **Code:** 200
  
    **Content:** `[]`

### Postman requests collection

You can import all available API calls to Postman using `postman_collection.json` file
